# README

![image](docs/img.png)

## Описание
Данное приложение на Vue 3 с использованием TypeScript представляет собой простой список дел (TODOs), который позволяет пользователю добавлять, редактировать и удалять задачи.

## Запуск приложения

1. Установите необходимые зависимости, выполнив следующую команду в корневой директории проекта:
```
npm install
```

2. Запустите приложение:
```
npm run serve
```

3. Откройте приложение в браузере, перейдя по ссылке:
```
http://localhost:8080/
```

## Функциональность

### Добавление задачи
Чтобы добавить новую задачу в список, введите текст задачи в поле ввода и нажмите Enter или кнопку "+".

### Удаление задачи
Чтобы удалить задачу из списка, щелкните по кнопке удаления  рядом с текстом задачи.

## Технологии
- Vue 3
- TypeScript

## Зависимости
Для работы приложения нужно установить следующие зависимости (перечислены в файле package.json):
- vue

## Автор
Семенов Владислав